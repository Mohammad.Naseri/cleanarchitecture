﻿using CleanArchitecture.Domain.Interfaces;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Infra.Data;
using CleanArchitecture.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICategoryRepository categoryRepository;
        private readonly IBookRepository bookRepository;
        private readonly IUnitOfWork unitOfWork;

        public HomeController(ILogger<HomeController> logger,
            ICategoryRepository categoryRepository,
            IBookRepository bookRepository,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            this.categoryRepository = categoryRepository;
            this.bookRepository = bookRepository;
            this.unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {

            try
            {
                //#region using Transaction
                ////unitOfWork.BeginTransaction();

                ////var category = new Category();
                ////category.Name = "Computer Science (Books)";
                ////categoryRepository.Add(category);
                ////categoryRepository.Save();

                //////throw new ArgumentNullException();

                ////Book book = new Book
                ////{
                ////    CategoryId = category.Id,
                ////    Name = "Introduction to Algorithms, 3rd Edition (The MIT Press) 3rd Edition",
                ////    Publisher = "MIT Press; 3rd edition (September 1, 2009)",
                ////    ISBN = "978-0262033848",
                ////    Language = "English",
                ////    Weight = 5,
                ////    Hardcover = 1292,
                ////    Summery = "Introduction to Algorithms, the 'bible' of the field, is a comprehensive textbook covering the full spectrum of modern algorithms: from the fastest algorithms and data structures to polynomial-time algorithms for seemingly intractable problems, from classical algorithms in graph theory to special algorithms for string matching, computational geometry, and number theory. The revised third edition notably adds a chapter on van Emde Boas trees, one of the most useful data structures, and on multithreaded algorithms, a topic of increasing importance."
                ////};
                ////bookRepository.Add(book);
                ////bookRepository.Save();

                ////unitOfWork.Commit();
                //#endregion

                //#region unitOfWork SaveChanges
                //var category = new Category();
                //category.Name = "Computer Science (Books)";
                //unitOfWork.GetRepository<Category>().Add(category);
                //unitOfWork.GetRepository<Category>().Save();

                ////throw new ArgumentNullException();

                //Book book = new Book
                //{
                //    CategoryId = category.Id,
                //    Title = "Introduction to Algorithms, 3rd Edition (The MIT Press) 3rd Edition",
                //    Publisher = "MIT Press; 3rd edition (September 1, 2009)",
                //    ISBN = "978-0262033848",
                //    Language = "English",
                //    Hardcover = 1292,
                //    Summery = "Introduction to Algorithms, the 'bible' of the field, is a comprehensive textbook covering the full spectrum of modern algorithms: from the fastest algorithms and data structures to polynomial-time algorithms for seemingly intractable problems, from classical algorithms in graph theory to special algorithms for string matching, computational geometry, and number theory. The revised third edition notably adds a chapter on van Emde Boas trees, one of the most useful data structures, and on multithreaded algorithms, a topic of increasing importance."
                //};
                //unitOfWork.GetRepository<Book>().Add(book);
                //unitOfWork.GetRepository<Book>().Save();
                //#endregion
            }
            catch (Exception ex)
            {
                //unitOfWork.Rollback();
                throw;
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

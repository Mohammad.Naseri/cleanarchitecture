﻿using AutoMapper;
using CleanArchitecture.Application.Features.Books;
using CleanArchitecture.Application.Features.Books.Commands;
using CleanArchitecture.Application.Features.Books.Queries;
using CleanArchitecture.Application.Features.Categories.Queries;
using CleanArchitecture.Application.Features.Categories.Queries.GetCategoriesList;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArchitecture.Mvc.Controllers
{
    public class BookController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public BookController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }
        public async Task<IActionResult> Index()
        {
            var query = new GetAllBooksQuery();
            return View(await _mediator.Send(query));
        }

        public async Task<IActionResult> Details(int id)
        {
            var query = new GetBookByIdQuery() { Id = id };
            return View(await _mediator.Send(query));
        }

        public IActionResult Create()
        {
            var createBookCommand = new CreateBookCommand();
            createBookCommand.Categories = GetAllCategories();
            return View(createBookCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateBookCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _mediator.Send(command);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes.");
            }
            return View(command);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var getBookByIdResponseModel = await _mediator.Send(new GetBookByIdQuery() { Id = id });
            var updateBookCommand = _mapper.Map<UpdateBookCommand>(getBookByIdResponseModel);
            updateBookCommand.Categories = GetAllCategories();

            return View(updateBookCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UpdateBookCommand command)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _mediator.Send(command);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to save changes.");
            }
            return View(command);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _mediator.Send(new DeleteBookCommand() { Id = id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to delete. ");
            }

            return RedirectToAction(nameof(Index));
        }

        private List<CategoryListVm> GetAllCategories()
        {
            var query = new GetCategoriesListQuery();
            return _mediator.Send(query).Result.ToList();
        }
    }
}

﻿using CleanArchitecture.Application.Interfaces;
using CleanArchitecture.Domain.Interfaces;
using CleanArchitecture.Infra.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Services
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;
        private readonly IUnitOfWork _UnitOfWork;
        private bool _disposed;

        public Service(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
        {
            _UnitOfWork = unitOfWork;
            _repository = repository;
        }
        public virtual void Add(TEntity entity)
        {
            _repository.Add(entity);
        }
        public virtual void Update(TEntity entity)
        {
            _repository.Update(entity);
        }
        public virtual void Remove(TEntity entity)
        {
            _repository.Remove(entity);
        }
        public virtual IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }
        public IQueryable<TEntity> GetAllQueryable()
        {
            return _repository.GetAll();
        }
        public virtual TEntity GetById(int id)
        {
            return _repository.GetById(id);
        }
        public virtual void Remove(int id)
        {
            _repository.Remove(GetById(id));
        }
        public virtual void RemoveCollection(List<int> ids)
        {
            foreach (var id in ids)
            {
                Remove(GetById(id));
            }
        }
        public virtual int Save()
        {
            return _UnitOfWork.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing && _UnitOfWork != null)
            {
                _UnitOfWork.Dispose();
            }
            _disposed = true;
        }

    }
}

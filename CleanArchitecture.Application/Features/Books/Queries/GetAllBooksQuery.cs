﻿using AutoMapper;
using CleanArchitecture.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books.Queries
{
    public class GetAllBooksQuery : IRequest<List<GetAllBooksResponseModel>>
    {
        public class GetAllBooksQueryHandler : IRequestHandler<GetAllBooksQuery, List<GetAllBooksResponseModel>>
        {
            private readonly IBookRepository bookRepository;
            private readonly IMapper mapper;

            public GetAllBooksQueryHandler(IBookRepository bookRepository, IMapper mapper)
            {
                this.bookRepository = bookRepository;
                this.mapper = mapper;
            }
            public async Task<List<GetAllBooksResponseModel>> Handle(GetAllBooksQuery request, CancellationToken cancellationToken)
            {
                var books = await bookRepository.GetAllAsyn();
                var dto = mapper.Map<List<GetAllBooksResponseModel>>(books);
                return dto;
            }
        }
    }
}

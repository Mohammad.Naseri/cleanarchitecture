﻿using AutoMapper;
using CleanArchitecture.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books.Queries
{
    public class GetBookByIdQuery : IRequest<GetBookByIdResponseModel>
    {
        public int Id { get; set; }

        public class GetBookByIdQueryHandler : IRequestHandler<GetBookByIdQuery, GetBookByIdResponseModel>
        {
            private readonly IBookRepository bookRepository;
            private readonly IMapper mapper;

            public GetBookByIdQueryHandler(IBookRepository bookRepository, IMapper mapper)
            {
                this.bookRepository = bookRepository;
                this.mapper = mapper;
            }
            public async Task<GetBookByIdResponseModel> Handle(GetBookByIdQuery request, CancellationToken cancellationToken)
            {
                var book = await bookRepository.GetByIdAsync(request.Id);
                var dto = mapper.Map<GetBookByIdResponseModel>(book);
                return dto;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books
{
    public class GetBookByIdResponseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public int Copyright { get; set; }
        public string Summery { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        [Display(Name = "Number of Pages")]
        public int Hardcover { get; set; }
        [Display(Name = "Number of Copies")]
        public int CopyNum { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
    }
}

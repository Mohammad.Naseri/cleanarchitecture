﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books
{
    public class GetAllBooksResponseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public int Copyright { get; set; }
        public string Summery { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        public int Hardcover { get; set; }
        public decimal CopyNum { get; set; }
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
    }
}

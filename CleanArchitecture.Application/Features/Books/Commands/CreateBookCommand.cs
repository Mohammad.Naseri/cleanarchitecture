﻿using AutoMapper;
using CleanArchitecture.Application.Features.Categories.Queries.GetCategoriesList;
using CleanArchitecture.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books.Commands
{
    public class CreateBookCommand : IRequest<int>
    {
        public string Title { get; set; }
        public string ISBN { get; set; }
        public int Copyright { get; set; }
        public string Summery { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        [Display(Name = "Number of Pages")]
        public int Hardcover { get; set; }
        [Display(Name = "Number of Copies")]
        public int CopyNum { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
        public List<CategoryListVm> Categories { get; set; }

        public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, int>
        {
            private readonly IBookRepository bookRepository;
            private readonly IMapper mapper;

            public CreateBookCommandHandler(IBookRepository bookRepository, IMapper mapper)
            {
                this.bookRepository = bookRepository;
                this.mapper = mapper;
            }
            public async Task<int> Handle(CreateBookCommand request, CancellationToken cancellationToken)
            {
                Domain.Entities.Book book = mapper.Map<Domain.Entities.Book>(request);
                bookRepository.Add(book);
                return await bookRepository.SaveAsync();
            }
        }
    }
}

﻿using CleanArchitecture.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books.Commands
{
    public class DeleteBookCommand : IRequest<int>
    {
        public int Id { get; set; }

        public class DeleteBookCommandHandler : IRequestHandler<DeleteBookCommand, int>
        {
            private readonly IBookRepository bookRepository;

            public DeleteBookCommandHandler(IBookRepository bookRepository)
            {
                this.bookRepository = bookRepository;
            }
            public async Task<int> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
            {
                var book = await bookRepository.GetByIdAsync(request.Id);
                if (book != null)
                {
                    bookRepository.Remove(book);
                    return await bookRepository.SaveAsync();
                }
                return 0;
            }
        }
    }
}

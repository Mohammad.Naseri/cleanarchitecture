﻿using AutoMapper;
using CleanArchitecture.Application.Features.Categories.Queries.GetCategoriesList;
using CleanArchitecture.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Books.Commands
{
    public class UpdateBookCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public int Copyright { get; set; }
        public string Summery { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        [Display(Name = "Number of Pages")]
        public int Hardcover { get; set; }
        [Display(Name = "Number of Copies")]
        public int CopyNum { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
        public List<CategoryListVm> Categories { get; set; }

        public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand, int>
        {
            private readonly IBookRepository bookRepository;
            private readonly IMapper mapper;

            public UpdateBookCommandHandler(IBookRepository bookRepository, IMapper mapper)
            {
                this.bookRepository = bookRepository;
                this.mapper = mapper;
            }
            public async Task<int> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
            {
                var book = await bookRepository.GetByIdAsync(request.Id);
                if (book == null)
                    return default;

                book.Title = request.Title;
                book.ISBN = request.ISBN;
                book.CategoryId = request.CategoryId;
                book.Publisher = request.Publisher;
                book.Summery = request.Summery;
                book.ImageUrl = request.ImageUrl;
                book.Hardcover = request.Hardcover;
                book.CopyNum = request.CopyNum;
                book.Copyright = request.Copyright;
                //book = mapper.Map<Domain.Models.Book>(request);

                bookRepository.Update(book);
                return await bookRepository.SaveAsync();
            }
        }
    }
}

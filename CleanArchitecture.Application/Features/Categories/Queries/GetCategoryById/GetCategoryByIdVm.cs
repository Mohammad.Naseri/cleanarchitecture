﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Features.Categories.Queries.GetCategoryById
{
    public class GetCategoryByIdVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

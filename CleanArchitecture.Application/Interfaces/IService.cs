﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Interfaces
{
    public interface IService<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> GetAllQueryable();
        TEntity GetById(int id);
        void Remove(int id);
        void RemoveCollection(List<int> ids);
        int Save();
    }
}

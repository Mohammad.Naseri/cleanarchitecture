﻿using CleanArchitecture.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infra.Data.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorBook> AuthorBooks { get; set; }
        public DbSet<Borrower> Borrower { get; set; }
        public DbSet<BookBorrower> BookBorrowers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Category>().HasData(
            new Category { Id = 1, Name = "Arts & Music" },
            new Category { Id = 2, Name = "Biographies" },
            new Category { Id = 3, Name = "Business" },
            new Category { Id = 4, Name = "Computers & Tech" },
            new Category { Id = 5, Name = "Cooking" }
            );

            builder.Entity<Book>().HasData(
                new Book { Id=1,CategoryId=4,Language="English",Hardcover= 464, ISBN = "978-0132350884", Publisher= "Prentice Hall",CopyNum=10,ImageUrl= "https://hackr.io/blog/uploads/images/clean-code-a-handbook-of-agile-software-craftsmanship-1st-edition.jpg", Title= "Clean Code: A Handbook of Agile Software Craftsmanship" },
                new Book { Id=2,CategoryId=4,Language="English",Hardcover= 1292, ISBN= "978-0262033848", Publisher= "The MIT Press",CopyNum=12,ImageUrl= "https://hackr.io/blog/uploads/images/introduction-to-algorithms-eastern-economy-edition.jpg",Title= "Introduction to Algorithms" },
                new Book { Id=3,CategoryId=4,Language="English",Hardcover= 657, ISBN = "978-0262510875", Publisher= "The MIT Press",CopyNum=13,ImageUrl= "https://hackr.io/blog/uploads/images/structure-and-interpretation-of-computer-programs-2nd-edition-mit-electrical-engineering-and-computer-science1.jpg", Title= "Structure and Interpretation of Computer Programs (SICP)" },
                new Book { Id=4,CategoryId=4,Language="English",Hardcover= 694, ISBN = "9780596007126", Publisher= "O’Reilly Media", CopyNum=12,ImageUrl= "https://hackr.io/blog/uploads/images/head-first-design-patterns-a-brain-friendly-guide-1st-edition.jpg", Title= "Head First Design Patterns: A Brain-Friendly Guide" },
                new Book { Id=5,CategoryId=4, Language = "English",Hardcover= 448, ISBN= "978-0134757599", Publisher= "Addison-Wesley Professional; 2nd edition (November 30, 2018)", CopyNum=12,ImageUrl= "https://hackr.io/blog/uploads/images/refactoring-improving-the-design-of-existing-code-2nd-edition-addison-wesley-signature-series-fowler-2nd-edition.jpg", Title= "Refactoring: Improving the Design of Existing Code" }
                );

            base.OnModelCreating(builder);
        }
    }
}

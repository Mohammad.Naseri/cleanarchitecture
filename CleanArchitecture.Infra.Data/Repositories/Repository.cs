﻿using CleanArchitecture.Domain.Interfaces;
using CleanArchitecture.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infra.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected AppDbContext _dbContext;

        public Repository(AppDbContext context)
        {
            _dbContext = context;
        }

        public T Add(T t)
        {
            _dbContext.Set<T>().Add(t);
            return t;
        }
        public void Remove(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
        }
        public T Update(T t)
        {
            if (t == null)
                return null;

            _dbContext.Entry(t).State = EntityState.Modified;
            return t;
        }
        public IQueryable<T> GetAll()
        {
            return _dbContext.Set<T>();
        }
        public async Task<IEnumerable<T>> GetAllAsyn()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }
        public T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }
        public async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }
        public async Task<int> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
        private bool disposed = false;
        protected void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this.disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

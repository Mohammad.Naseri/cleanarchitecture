﻿using CleanArchitecture.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infra.Data
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Repository<T> GetRepository<T>() where T : class;
        void BeginTransaction();
        void Commit();
        void Rollback();
        //Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task CommitAsync();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Add(T entity);
        T Update(T entity);
        void Remove(T entity);
        IQueryable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsyn();
        T GetById(int id);
        Task<T> GetByIdAsync(int id);
        void Save();
        Task<int> SaveAsync();
        void Dispose();
    }
}

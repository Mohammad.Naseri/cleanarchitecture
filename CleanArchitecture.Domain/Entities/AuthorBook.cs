﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Entities
{
    public class AuthorBook:BaseEntity
    {
        public virtual Book Book { get; set; }
        public int BookId { get; set; }
        public virtual Author Author { get; set; }
        public int AuthorId { get; set; }
    }
}

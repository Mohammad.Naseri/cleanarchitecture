﻿using CleanArchitecture.Application.Interfaces;
using CleanArchitecture.Application.Services;
using CleanArchitecture.Domain.Interfaces;
using CleanArchitecture.Infra.Data;
using CleanArchitecture.Infra.Data.Context;
using CleanArchitecture.Infra.Data.Repositories;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infra.IoC
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Application Layer
            services.AddScoped<IBookService, BookService>();

            //Infra.Data Layer
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IBookRepository, BookRepository>();

            //services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            //MeditR
            var assembly = typeof(Application.Features.Books.Queries.GetAllBooksQuery).Assembly;
            services.AddMediatR(assembly);
            
            //AddAutoMapper
            services.AddAutoMapper(assembly);
        }
    }
}
